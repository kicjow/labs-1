﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(ICzłowiek);

        public static Type ISub1 = typeof(IMężczyzna);
        public static Type Impl1 = typeof(Mężczyzna);
        
        public static Type ISub2 = typeof(IKobieta);
        public static Type Impl2 = typeof(Kobieta);


        public static string baseMethod = "Mow";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Idz";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Biegnij";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Persony";
        public static string collectionConsumerMethod = "OpowiedzcieOSobie";

        #endregion

        #region P3

        public static Type IOther = typeof(IKosmita);
        public static Type Impl3 = typeof(Kosmita);

        public static string otherCommonMethod = "Mow";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}

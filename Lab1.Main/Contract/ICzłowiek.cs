﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
  public  interface ICzłowiek
    {
        string Mow();
    }
  public interface IMężczyzna : ICzłowiek
    {
        string Idz();
    }
  public interface IKobieta : ICzłowiek
    {
        string Biegnij();
    }

  public interface IKosmita
  {
       string Strzelaj();
       string Idz();
       string Mow();
  }
}

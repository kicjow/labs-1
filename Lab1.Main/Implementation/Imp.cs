﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Mężczyzna : IMężczyzna
    {
        string imie;
        public Mężczyzna()
        {
            imie = "czlowiek1";
        }
        public string Mow()
        {
            return "jestem" + this.imie;
        }

        public string Idz()
        {
            return this.imie;
        }

    }

    class Kobieta : IKobieta
    {
        string imie;
        public Kobieta()
        {
            imie = "czlowiek2";
        }

        public string Biegnij()
        {
            return this.imie;
        }

        public string Mow()
        {
            return "jestem inny " + this.imie;
        }

    }

    class Kosmita : IKosmita, IMężczyzna
    {
        string imie;
        public Kosmita()
        {
            this.imie = "kosmita";
        }
        public string Mow()
        {
            return "kosmita mowi";
        }


        public string Strzelaj()
        {
            return "pif paf";
        }

        public string Idz()
        {
            return "kosmita ide";
        }
        string ICzłowiek.Mow()
        {
            return "kosmita" + this.imie;
        }
        string IKosmita.Mow()
        {
            return "blabla";
        }






    }
}

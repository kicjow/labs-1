﻿using System;
using System.Collections;
using Lab1.Contract;
using Lab1.Implementation;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {
        //ISI2: IBase = ICzłowiek,
        //ISub1 = IMężczyzna,
        //ISub2 = IKobieta,
        //IOther = IKosmita

        public static ICollection<ICzłowiek> Persony()
        {
            List<ICzłowiek> Lista = new List<ICzłowiek> { };

            Lista.Add(new Mężczyzna());
            Lista.Add(new Kobieta());
            Lista.Add(new Kosmita());

            return Lista;
        }

        public static void OpowiedzcieOSobie(ICollection<ICzłowiek> opowiadający)
        {
            foreach (var item in opowiadający)
            {
                Console.WriteLine(item.Mow());
            }
        }
     

        static void Main(string[] args)
        {

            OpowiedzcieOSobie(Persony());
            Console.ReadKey();
          
        }
    }
}
